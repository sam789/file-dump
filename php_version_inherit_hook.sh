#!/bin/bash

# curl -s https://gitlab.com/brixly/file-dump/raw/master/php_version_inherit_hook.sh | bash

# Check if the hook / script already exists, if not, install it
if [ ! -f /usr/local/cpanel/3rdparty/bin/php_version_inherit_hook.sh ]; then
    wget -O /usr/local/cpanel/3rdparty/bin/php_version_inherit_hook.sh https://gitlab.com/brixly/file-dump/raw/master/php_version_inherit_hook.sh
    chmod 755 /usr/local/cpanel/3rdparty/bin/php_version_inherit_hook.sh
    /usr/local/cpanel/bin/manage_hooks add script /usr/local/cpanel/3rdparty/bin/php_version_inherit_hook.sh --manual --category PkgAcct --event Restore --stage=post
fi

tmpfile="$(mktemp -p /tmp php-version-hook-data-XXXXXXXX)"
cat "${1:-/dev/stdin}" > $tmpfile

cpanelusername=$(python -c "import sys, json; print json.load(open('$tmpfile'))['data']['user']")

rm -f $tmpfile

# Loop through domains for user, and set the PHP Version to Inherit
for domain in $(cat /etc/userdomains | grep -w $cpanelusername | awk -F":" '{print $1}')
do
/usr/sbin/whmapi1 php_set_vhost_versions vhost-0=$domain version=inherit
done
