#!/bin/bash

QUEUE_SIZE=$(exim -bpc)

if test $QUEUE_SIZE -gt 10000; then
    echo "2 EXIM_QUEUE - QUEUE HIGHER THAN 10K - $QUEUE_SIZE"
elif test $QUEUE_SIZE -gt 5000; then
    echo "1 EXIM_QUEUE - QUEUE CURRENTLY $QUEUE_SIZE"
else
    echo "0 EXIM_QUEUE - FINE"
fi