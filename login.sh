#!/bin/bash


# If there is no input then login to whm as root
if [ "$#" -eq 0 ] ; then
        whmapi1 create_user_session user=root service=whostmgrd
        exit 0
fi


# set some defaults
service=cpaneld
user=$(whoami)

# Explanation:
# t is not required but requires a value, used for specifying service, defaults to cpanel
# u is not required but requires a value, used for specifying user, defaults to script runner
# a is not required and requires a value, used for specifying application, cPanel default to home
# h shows help output and takes no value

while getopts "t:u:a:h" arg; do
    case $arg in
        t) # Specifies service login
            case ${OPTARG} in
                whm | WHM | reseller | r) # Login to WHM
                    service=whostmgrd
                    ;;
                cpanel | cPanel | panel | c) # Login to cPanel
                    service=cpaneld
                    ;;
                webmail | mail | w) # Login to Webmail
                    service=webmaild
                    ;;
                *)
                    service=cpaneld # If not found we login to cPanel
                    ;;
            esac
            ;;
        u) # The login we're using
            user=${OPTARG}
            ;;
        a) # The application to access
            case ${OPTARG} in
                filemanager | FileManager | Filemanager) # Login directly to FileManager
                    app="app=FileManager_Home"
                    ;;
                mysql | database) #Login directly to MySQL Databases page
                    app="app=Database_MySQL"
                    ;;
                postgres | pgsql | postgresql) # Login directly to Postgresql page
                    app="app=sql_server_pgsql"
                    ;;
                phpmyadmin | phpMyAdmin) # Login directly to phpMyAdmin
                    app="app=phpMyAdmin"
                    ;;
                *) # If you're specifying the literal function, defaults to HOME if it doesn't exist (cpanel feature)
                    app="app=${OPTARG}"
                    ;;
            esac
            ;;
        h | *) # Display help
            usage
            exit 0
            ;;
    esac
done

url=$(whmapi1 create_user_session user=$user service=$service $app --output=json) # Make the WHMAPI1 call
# Check to see if the call fails
if [[ $(echo $url | jq '.metadata | .result') == 1 ]]; then 
    # if not just paste the SSO URL
    echo $(echo $url | jq '.data | .url')
    exit 0
else
    # if it fails say which service it failed to login to and provide the reason
    echo "Failed to login to $service"
    echo $(echo $url | jq '.metadata | .reason')
    exit 1
fi
exit 1
