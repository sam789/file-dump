#!/bin/bash

HOSTNAME=`hostname`
SHORT_HOSTNAME=`echo "$HOSTNAME" | cut -d'.' -f1`
ART=`figlet $SHORT_HOSTNAME`
URL="https://$HOSTNAME:2083"
INTERFACE="cPanel"
​
if [[ `cldetect --detect-cp-nameonly` == "DirectAdmin" ]]; then
	INTERFACE="DirectAdmin"
	URL="https://$HOSTNAME:2222"
fi
​
cat > /etc/motd <<EOF
$ART
Welcome to $HOSTNAME, hope you enjoy your stay!
​
You can view the $INTERFACE interface via: $URL
​
EOF
