#!/usr/bin/python3

import json
import subprocess
import sys

# Usage of command
def help():
    sys.exit("Usage: /scripts/imunify-whitelist command [domain/email] target\nAvailable commands:\nlist\nadd\ndelete")

def list():
    sys.exit(subprocess.getoutput("ie-cli wl-senders list"))

if len(sys.argv) == 1:
    help()
# Check if they are looking for help 
elif sys.argv[1] == "help" or sys.argv[1] == "--help" or sys.argv[1] == "-h":
    help()
# List if they ask for list
elif sys.argv[1] == "list":
    list()
# Check for add/delete as first command
elif (sys.argv[1] != "add" and sys.argv[1] != "delete"):
    help()
# Check if they are correctly specifying domain/email
elif (sys.argv[2] != "domain" and sys.argv[2] != "email"):
    help()
# We need three arguments at this point
elif len(sys.argv) != 4:
    help()

# The target to be whitelisted
target = sys.argv[3]

# Check if it's an email address
if '@' in target:
    # Get just the domain if so
    split = target.split('@')
    domain = split[1]
    email = split[0]
else:
    domain = target

# Grab the domain owner
owner = json.loads(subprocess.getoutput(f"whmapi1 getdomainowner domain={domain} --output=json"))

# If there's no owner this comes back blank, i.e. domain does not exist
if not owner['data']['user']:
    sys.exit("Failed! Please check that the entered domain exists.")

# Get the email accounts 
accounts = json.loads(subprocess.getoutput(f"whmapi1 list_pops_for user={owner['data']['user']} --output=json"))['data']['pops']

# Check if the domain or email account is in the list of accounts
if target in accounts and email:
    print(subprocess.getoutput(str("ie-cli wl-senders %s -i \'[{\"type\": \"%s\", \"value\": \"%s\"}]\'") % (sys.argv[1], sys.argv[2], target)))
elif 'email' not in locals():
    print(subprocess.getoutput(str("ie-cli wl-senders %s -i \'[{\"type\": \"%s\", \"value\": \"%s\"}]\'") % (sys.argv[1], sys.argv[2], target)))
else:
    sys.exit("Failed! Email account not found.")
