#!/bin/bash

if [[ "$1" = "help" ]] || [[ "$1" = "--help" ]] || [[ "$1" = "-h" ]]; then
    printf "Usage:\t/scripts/repair_account.sh [-dpf] account\n-d = Repair Database, change database password and update AnonymousFox user to admin including set random password\n-p = Set random cPanel account Password\n-f = Bypass .contactmail AnonymousFox check and run script\n"
    exit
fi

if [ "$#" -eq 2 ]; then
    USERNAME=$2
else
    USERNAME=$1
fi

if [ $(whmapi1 accountsummary user=$USERNAME | grep result | awk '{print $2}') -ne 1 ]; then
    echo "Account $USERNAME does not exist"
    exit 1
fi

while getopts dpf flag; do
    case "${flag}" in
        d) DATABASE=1;;
        p) PASSWORD=1;;
        f) FORCE=1;;
    esac
done

function repair {
    echo "Running shadow fix on $USERNAME"
    domain_list=$(grep -w $USERNAME /etc/userdomains | cut -d: -f1)
        for domain in $domain_list; do
        if test -f "/home/$USERNAME/etc/$domain/shadow.roottn.bak"; then
            mv /home/$USERNAME/etc/$domain/shadow /home/$USERNAME/etc/$domain/shadow.old
            mv /home/$USERNAME/etc/$domain/shadow.roottn.bak /home/$USERNAME/etc/$domain/shadow
        fi
        sed -i '/^anonymousfox/d' /home/$USERNAME/etc/$domain/shadow
        sed -i '/^smtpfox/d' /home/$USERNAME/etc/$domain/shadow
        sed -i '/^-/d' /home/$USERNAME/etc/$domain/shadow
        mv /home/$USERNAME/etc/$domain/\@pwcache /home/$USERNAME/etc/$domain\/@pwcache.old
    done
    
    sed -i "s/$(cat /home/$USERNAME/.contactemail)/$(whmapi1 accountsummary user=$USERNAME | grep email: | cut -d: -f2 | xargs)/g" /home/$USERNAME/.cpanel/contactinfo
    sed -i "s/$(cat /home/$USERNAME/.contactemail)/$(whmapi1 accountsummary user=$USERNAME | grep email: | cut -d: -f2  | xargs)/g" /home/$USERNAME/.contactemail

    if [[ $DATABASE -eq 1 ]]; then
        echo "Running database fix for $USERNAME"
        FILE=/home/$USERNAME/public_html/wp-config.php
        if test -f "$FILE"; then
            PASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
            DB_USER=$(grep DB_USER $FILE | awk -F"'" '{print $4}')
            DB_PASS=$(grep DB_PASSWORD $FILE | awk -F"'" '{print $4}')
            sed -i "/DB_PASSWORD/c\define('DB_PASSWORD', '$PASS');" $FILE
            uapi --user=$USERNAME Mysql set_password user=$DB_USER password=$PASS
        fi
        su -s /bin/bash - $USERNAME -c "wp user update AnonymousFox --user_pass=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1) --path=/home/$USERNAME/public_html"
        su -s /bin/bash - $USERNAME -c "wp search-replace 'AnonymousFox' 'admin' --path=/home/$USERNAME/public_html"        
    fi

    if [[ $PASSWORD -eq 1 ]]; then
        echo "Setting cPanel password for $USERNAME"
        whmapi1 passwd user=$USERNAME password=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
    fi
exit 0
}

if [[ $FORCE -eq 1 ]]; then
    repair
elif ! grep -iq anonymousfox /home/$USERNAME/.contactemail; then
    while true; do
        read -p "AnonymousFox not found in contactemail, proceed anyway? (y/n) "
        case $REPLY in
            y|Y|yes|Yes ) repair
                          break;;

            n|N|no|No   ) exit 1;;

            *           ) echo "Please answer yes or no."
        esac
    done
else
    repair
fi
