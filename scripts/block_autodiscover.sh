#!/bin/bash

USER=$1

if whmapi1 listaccts | grep user: | awk '{print $2}' | grep -w $USER
then
    mkdir /etc/apache2/conf.d/userdata/std/2_4/$USER
    mkdir /etc/apache2/conf.d/userdata/ssl/2_4/$USER

echo "<Location /autodiscover>  
 Deny from All
</Location>

<Location /Autodiscover>
 Deny from All
</Location>
" > /etc/apache2/conf.d/userdata/std/2_4/$USER/autodiscover.conf

echo "<Location /autodiscover>  
 Deny from All
</Location>

<Location /Autodiscover>
 Deny from All
</Location>
" > /etc/apache2/conf.d/userdata/ssl/2_4/$USER/autodiscover.conf

    /usr/local/cpanel/scripts/rebuildhttpdconf
    if [[ $(echo $?) == 0 ]]
        then
        echo "Apache conf build passed"
    else
        echo "Apache conf rebuild failed"
    fi
else
    echo "$USER does not exist"
fi
