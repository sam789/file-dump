#!/usr/bin/python3

import json
import subprocess
import sys

domain=sys.argv[1]

data=json.loads(subprocess.getoutput(f"whmapi1 domainuserdata domain={domain} --output=json"))

if data['metadata']['result'] == 0:
    sys.exit(f"Failed! Error was {data['metadata']['reason']}")
else:
    print(data['data']['userdata']['documentroot']) 