#!/usr/bin/python3

import json
import subprocess
import sys

user=sys.argv[1]

resellerdata=json.loads(subprocess.getoutput(f"whmapi1 resellerstats user={user} --output=json"))

if resellerdata['metadata']['result'] == 0:
    sys.exit(f"Failed! Error is {resellerdata['metadata']['reason']}")

for account in resellerdata['data']['reseller']['acct']:
    print(account['user'])
