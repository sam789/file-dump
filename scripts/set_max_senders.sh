#!/bin/bash

# bash <(curl -s https://gitlab.com/brixly/file-dump/raw/master/scripts/set_max_senders.sh)

wget -O /scripts/set_max_senders.sh https://gitlab.com/brixly/file-dump/raw/master/scripts/set_max_senders.sh
chmod 655 /scripts/set_max_senders.sh

# Correct Account-specific Max hourly emails per domain settings
grep -rl "MAX_EMAIL_PER_HOUR=" /var/cpanel/users | xargs sed -i "s/MAX_EMAIL_PER_HOUR=.*/MAX_EMAIL_PER_HOUR=150/g"
grep -rl "MAX_DEFER_FAIL_PERCENTAGE=" /var/cpanel/users | xargs sed -i "s/MAX_DEFER_FAIL_PERCENTAGE=.*/MAX_DEFER_FAIL_PERCENTAGE=125/g"

/usr/local/cpanel/scripts/updateuserdomains

# Set Tweak Setting Values
# Set the maximum number of emails per hour
whmapi1 set_tweaksetting key=maxemailsperhour value=150

# Set the defer cutoff point as a percentage (recommended: 125%)
whmapi1 set_tweaksetting key=email_send_limits_defer_cutoff value=125

# Set the outbound SPAM detection 'action' to 'block' any further emails
whmapi1 set_tweaksetting key=email_outbound_spam_detect_action value=block

# Prevent the user 'nobody' from sending mail
whmapi1 set_tweaksetting key=nobodyspam value=1

# Maximum percentage of failed or deferred messages a domain may send per hour
whmapi1 set_tweaksetting key=email_send_limits_max_defer_fail_percentage value=25

# Number of failed or deferred messages a domain may send before outgoing mail is suspended in an hour
whmapi1 set_tweaksetting key=email_send_limits_min_defer_fail_to_trigger_protection value=25

# Monitor the number of unique recipients per hour to detect potential spammers and notify clients
whmapi1 set_tweaksetting key=email_outbound_spam_detect_enable value=1

# Reject / Block outgoing mail for a mail account when potential SPAM actions have been hit
whmapi1 set_tweaksetting key=email_outbound_spam_detect_action value=block

# The number of unique recipients per hour to trigger potential spammer notification
whmapi1 set_tweaksetting key=email_outbound_spam_detect_threshold value=250

# The number of emails a domain may send per day before receiving a system notification
whmapi1 set_tweaksetting key=emailsperdaynotify value=500

# Initial default/catch-all forwarder destination (required for SpamExperts - 'fail')
whmapi1 set_tweaksetting key=defaultmailaction value=fail

# Set the default mailbox storage to 'mdbox' to reduce inodes usage
whmapi1 set_tweaksetting key=mailbox_storage_format value=mdbox

# Ensure that the disk usage calculations also include mailman data
whmapi1 set_tweaksetting key=disk_usage_include_mailman value=0

# Ensure that 'mailman' relayed emails are included in the outbound sending quota
whmapi1 set_tweaksetting key=email_send_limits_count_mailman value=0

# Disable 'archiving' support
whmapi1 set_tweaksetting key=emailarchive value=0

# Purge hordes 'user cache' every 'x' days
whmapi1 set_tweaksetting key=horde_cache_empty_days value=7

# Disable SpamAssassin
whmapi1 set_tweaksetting key=skipspamassassin value=0

# Disable SMTP Tweak (restrict outbound SMTP connections)
whmapi1 set_tweaksetting key=smtpmailgidonly value=0

sed -i "s/: .*/: 150,60,15/g" /etc/email_send_limits

find /var/cpanel/packages/ -type f -exec sed -i "/MAX_DEFER_FAIL_PERCENTAGE/c\MAX_DEFER_FAIL_PERCENTAGE=125" {} ';'
find /var/cpanel/packages/ -type f -exec sed -i "/MAX_EMAIL_PER_HOUR/c\MAX_EMAIL_PER_HOUR=150" {} ';'

# Create Cronjob
echo "0 */12 * * * /bin/bash /scripts/set_max_senders.sh > /dev/null 2>&1" > /etc/cron.d/set_max_senders
