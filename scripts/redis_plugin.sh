#!/usr/bin/bash

dnf -y install https://linux-mirrors.fnal.gov/linux/centos/8/configmanagement/x86_64/ansible-29/Packages/a/ansible-2.9.27-1.el8.noarch.rpm

yum -y install https://github.com/AnoopAlias/AUTOM8N/raw/ndeploy4/nDeploy-release-centos-1.0-8.noarch.rpm

yum -y --enablerepo=autom8redis install Autom8Redis

wget -O /opt/Autom8Redis/redis.png https://gitlab.com/sam789/file-dump/-/raw/master/scripts/redis.png

sed -i 's/xtendweb.png/redis.png/' /opt/Autom8Redis/install.json

sed -i 's/"name" : "Autom8Redis"/"name" : "Redis"/' /opt/Autom8Redis/install.json

/usr/local/cpanel/scripts/uninstall_plugin /opt/Autom8Redis --theme=paper_lantern
/usr/local/cpanel/scripts/uninstall_plugin /opt/Autom8Redis --theme=jupiter
/usr/local/cpanel/scripts/install_plugin /opt/Autom8Redis --theme=paper_lantern
/usr/local/cpanel/scripts/install_plugin /opt/Autom8Redis --theme=jupiter