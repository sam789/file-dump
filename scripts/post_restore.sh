#!/bin/bash

account=$(echo $@ | awk '{print $4}')

cagefsctl --remount ${account}
selectorctl --set-user-current=7.2 --user=${account}
