#!/bin/bash

# bash <(curl -s https://gitlab.com/brixly/file-dump/raw/master/scripts/enable_cagefs.sh)

cagefsctl --enable-all

# Create Cronjob
wget -O /scripts/enable_cagefs.sh https://gitlab.com/brixly/file-dump/raw/master/scripts/enable_cagefs.sh
chmod 655 /scripts/enable_cagefs.sh
echo "0 * * * * /bin/bash /scripts/enable_cagefs.sh > /dev/null 2>&1" > /etc/cron.d/enable_cagefs
