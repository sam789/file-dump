#!/usr/bin/python3

import json
import subprocess
import sys

user=sys.argv[1]

resellerdata=json.loads(subprocess.getoutput(f"whmapi1 resellerstats user={user} --output=json"))

if resellerdata['metadata']['result'] == 0:
    sys.exit(f"Failed! Error is {resellerdata['metadata']['reason']}")

domain_list=json.loads(subprocess.getoutput("whmapi1 get_domain_info --output=json"))
for domain in domain_list['data']['domains']:
    if domain['user_owner'] == user:
        print(domain['domain'])