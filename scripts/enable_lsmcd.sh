# /bin/bash
yum groupinstall "Development Tools" -y
yum install autoconf automake zlib-devel openssl-devel expat-devel pcre-devel libmemcached-devel cyrus-sasl* -y
cd /root
git clone https://github.com/litespeedtech/lsmcd.git
cd lsmcd
./fixtimestamp.sh
./configure CFLAGS=" -O3" CXXFLAGS=" -O3"
make
sudo make install
sudo systemctl start lsmcd
sudo systemctl enable lsmcd

# Install SASL and LSMCD Plugin
cd /root
git clone https://github.com/rperper/lsmcd_cpanel_plugin.git
cd lsmcd_cpanel_plugin/res/lsmcd_usermgr
./install.sh

wget -O /scripts/bulk_enable_sasl.sh https://gitlab.com/brixly/file-dump/-/raw/master/scripts/bulk_enable_sasl.sh
chmod 655 /scripts/bulk_enable_sasl.sh

wget -O /scripts/sasl_add_user.sh https://gitlab.com/brixly/file-dump/-/raw/master/sasl_add_user.sh
chmod 655 /scripts/sasl_add_user.sh

    /usr/local/cpanel/bin/manage_hooks \
    add script /scripts/bulk_enable_sasl.sh \
    --stage post \
    --category Whostmgr \
    --event Accounts::Create \
    --manual
if ! grep -q string file.txt; then
    echo "CACHED.USESASL=TRUE" >> /usr/local/lsmcd/conf/node.conf
else
    sed -i 's/.*CACHED.USESASL.*/CACHED.USESASL=TRUE/' /usr/local/lsmcd/conf/node.conf
fi
service lsmcd restart
bash /scripts/bulk_enable_sasl.sh
chmod 666 /etc/sasldb2
