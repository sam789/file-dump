#!/usr/bin/python3

import json
import subprocess
import sys
import os
import shutil

if len(sys.argv) != 2:
    sys.exit("Error! Takes one arg only who's a user on the server!")

if not os.path.isdir(f"/home/{sys.argv[1]}"):
    sys.exit(f"Either user {sys.argv[1]} does not exist or their home directory is missing!")

latest = json.loads(subprocess.getoutput("curl -s https://api1.whmcs.com/download/latest?type=stable"))

url = latest['url']

filename = url.split("/")[-1]

subprocess.getoutput(f"wget {url} --quiet -O /home/{sys.argv[1]}/{filename}")

shutil.chown(f"/home/{sys.argv[1]}/{filename}", sys.argv[1], sys.argv[1])

print(f"Completed! File path is\n/home/{sys.argv[1]}/{filename}")
