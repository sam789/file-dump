#/bin/bash

# Creates backups of all accounts, excluding the 'homedir', to simplify restores

for user in $(ls /var/cpanel/users); do /scripts/pkgacct --allow-override $user --skiphomedir /backup/ && mv -f /backup/cpmove-$user.tar.gz /mnt/backups/$(hostname -s)/.; done;