#!/usr/bin/python3

import subprocess
import json
import os

domains = json.loads(subprocess.getoutput("whmapi1 --output=json get_domain_info"))

for domain in domains['data']['domains']:
    try:
        users = [name for name in os.listdir(f"/home/{domain['user']}/mail/{domain['domain']}") if os.path.isdir(f"/home/{domain['user']}/mail/{domain['domain']}/{name}")]
        for user in users:
            subprocess.getoutput(f"doveadm purge -u {user}@{domain['domain']}")
    except:
        continue